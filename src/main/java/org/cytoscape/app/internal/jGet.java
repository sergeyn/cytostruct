package org.cytoscape.app.internal;

import java.io.*;
import java.net.*;

class jGet implements Runnable {

    @Override
    public void run() {
        String fromClient="";
       // String toClient;
        ServerSocket server;
        Socket client = null;
        BufferedReader in;
		PrintWriter out;
		try { server = new ServerSocket(8982); }
		catch(IOException e) { return; }
        System.out.println("wait for connection on port 8982");
        boolean run = true;
        while(run) {
			try{
				client = server.accept();
				System.out.println("got connection on port 8982");
				in = new BufferedReader(new InputStreamReader(client.getInputStream()));
				out = new PrintWriter(client.getOutputStream(),true);
				fromClient = in.readLine();
				client.close();
			}
			catch(IOException e) { run = false; }
			System.out.println("received: " + fromClient);
        }
        System.out.println("socket closing");
    }
    
public static void smain(String args[]) throws Exception {
		(new Thread(new jGet())).start();
	}
}
