package org.cytoscape.app.internal;

import java.util.HashMap;
import java.util.Vector;
import java.util.Arrays;
import java.lang.Exception;
import javax.swing.JOptionPane;

class VisManager {
	public enum DataType { DEDGE, MEDGE, MNODE};
	
	private String entPath;
	private String pymol;
	private String alignScript;
	
	private HashMap<Integer,Vector<EdgeInfoBase> > domainEdges;
	private HashMap<Integer,Vector<EdgeInfoBase> > motifEdges;
	private HashMap<Integer,Vector<EdgeInfoBase> > motifNodes;
	
	public VisManager(){ this.entPath = this.pymol = this.alignScript = null; }
	public VisManager(String entPath, String pymol, String align){ this.entPath = entPath; this.pymol = pymol; this.alignScript = align; }
	public void setConfig(String entPath, String pymol, String align) { this.entPath = entPath; this.pymol = pymol; this.alignScript = align; }
	
	public Vector<EdgeInfoBase> get(Integer id, DataType t) { 
		switch(t) {
			case DEDGE:
				return domainEdges == null ? null : domainEdges.get(id); 
			case MEDGE:
				return motifEdges == null ? null : motifEdges.get(id); 
			case MNODE:
				return motifNodes == null ? null : motifNodes.get(id); 
			default: 
				return null;
		}
	}
	
	public void runPyMol(String dNode) {
		EdgeInfoBase info = new DomainNode(dNode);
		(new Thread(new WriteTemp(info,entPath,pymol,alignScript))).start();
	}
	public void runPyMol(Integer id, DataType type) {
		Vector<EdgeInfoBase> elements = get(id,type);
		if(elements == null || elements.size() == 0) return;
		Integer indexElement = 0;
		if(elements.size()>1) {
			String[] labels = new String[elements.size()];
			int i = 0;
			for(EdgeInfoBase eib : elements) {
				labels[i++] = eib.getLabel(i);
			}
			String s = (String)JOptionPane.showInputDialog(
						null,
						"Select:",
						"Selection dialog",
						JOptionPane.PLAIN_MESSAGE,
						null,
						labels,labels[0]);
			indexElement = Arrays.asList(labels).indexOf(s); 
			System.out.println(indexElement);
		}
		EdgeInfoBase info = elements.elementAt(indexElement);
		(new Thread(new WriteTemp(info,entPath,pymol,alignScript))).start();
	}
	
	public void loadData(String pathToZip) {
		motifEdges = EdgeInfoM.readFromFile(pathToZip,new EdgeInfoM(null), 2);
		motifNodes = EdgeInfo.readFromFile(pathToZip,new EdgeInfo(null),3);
		domainEdges =  EdgeInfo.readFromFile(pathToZip,new EdgeInfo(null),1);
		
		System.out.println("Loaded nodes/edges data rows:");
		System.out.println(domainEdges.size());
		System.out.println(motifEdges.size());
		System.out.println(motifNodes.size());
	}
	
/*	
	// test standalone
	public static void main(String[] args) {
		VisManager vsm = new VisManager("entpath","pymol","align");
		vsm.loadData("/home/sergeyn/Dropbox/code/cytoscape/plugin_data_raw/PLUGIN_DATA");
		vsm.runPyMol(2,VisManager.DataType.MEDGE);
		vsm.runPyMol(2,VisManager.DataType.MNODE);
		vsm.runPyMol(2,VisManager.DataType.DEDGE);
		vsm.runPyMol("d1");
	}
*/	

        private  class WriteTemp implements Runnable {
			private EdgeInfoBase ei;
			private String path;
			private String pymol;
			private String alignScript;
			WriteTemp(EdgeInfoBase ei, String path, String pymol, String alignScript) {
				this.ei = ei;
				this.path = path;
				this.pymol = pymol;
				this.alignScript = alignScript;
			}
			
			public void run() {
				String cmd =  ei.getCmd(path,pymol,alignScript); //pymol + " \"" + ei.getScriptFilePath(path,alignScript)+"\"";
				try {
					System.out.println(cmd);
					Thread.sleep(100);
					Runtime.getRuntime().exec(cmd);        
				} catch (Exception ex) {}			
			}		
	}	
}
