package org.cytoscape.app.internal;

import org.cytoscape.app.internal.core.ErrBuffer;

import java.util.ArrayList;
import java.util.List;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.stream.Collectors;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.cytoscape.application.swing.CyMenuItem;
import org.cytoscape.application.swing.CyNodeViewContextMenuFactory;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTableUtil;

public class NodeViewContextMenuBase implements CyNodeViewContextMenuFactory, ActionListener {

    private final ExternalRunner runner;
    private final ConfigManager dataRoot;
    private final String caption;

    public NodeViewContextMenuBase(ConfigManager dataRoot, ExternalRunner runner, String label) {
        super();
        this.dataRoot = dataRoot;
        this.caption = label;
        this.runner = runner;
    }

    @Override
    public CyMenuItem createMenuItem(CyNetworkView netView,
            View<CyNode> nodeView) {
        JMenuItem menuItem = new JMenuItem(this.caption);
        menuItem.addActionListener(this);
        CyMenuItem cyMenuItem = new CyMenuItem(menuItem, 0);
        return cyMenuItem;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //Get the selected nodes
        final CyApplicationManager manager = dataRoot.cyApplicationManager;
        List<CyNode> nodes;
        nodes = CyTableUtil.getNodesInState(manager.getCurrentNetwork(), "selected", true);

        final CyNetworkView networkView = manager.getCurrentNetworkView();
        final CyNetwork network = networkView.getModel();

        List<String> nodeLabels = new ArrayList<>();
        for(CyNode node : nodes) {
            nodeLabels.add(network.getRow(node).getRaw(CyNetwork.NAME).toString());
        }

        runner.execNode(caption, nodeLabels);
        ErrBuffer.showIfErr();
    }
}
