/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cytoscape.app.internal;

import java.util.List;

/**
 *
 * @author sergeyn
 */
public interface ExternalRunner {
    void execNode(String app, List<String> nodeLabels);
    void execEdge(String app, String edgeLabel, String left, String right);
}
