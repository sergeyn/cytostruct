package org.cytoscape.app.internal;

import org.cytoscape.app.internal.core.ApplicationSetup;
import org.cytoscape.app.internal.core.ErrBuffer;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JMenu;
import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.AbstractCyAction;
import org.cytoscape.service.util.AbstractCyActivator;
import org.cytoscape.session.events.SessionAboutToBeSavedListener;
import org.cytoscape.session.events.SessionLoadedListener;
import org.osgi.framework.BundleContext;

public class CyActivator extends AbstractCyActivator {

    private BundleContext bc;
    private ConfigManager dataRoot;
    private GenericRunner runner;

    @Override
    public void start(BundleContext context) throws Exception {
        bc = context; 
        CyApplicationManager cyApplicationManager = getService(context, CyApplicationManager.class);
        CyAppAdapter adapter = getService(bc, CyAppAdapter.class);
        if(dataRoot == null || false == dataRoot.isStore())
            dataRoot = new ConfigManager(cyApplicationManager, context, adapter, this);
    try {
        if(dataRoot.isStore()) {
            ErrBuffer.out("looks like setCfg flow");
        }
        else if(adapter.getCySessionManager().getCurrentSessionFileName() != null) {
            //we have a session in the air:
            //ErrBuffer.out(new File(adapter.getCySessionManager().getCurrentSessionFileName()).getAbsolutePath());
            dataRoot.setDir(adapter.getCySessionManager().getCurrentSessionFileName());
            dataRoot.getDataFromOpenSession(adapter.getCySessionManager().getCurrentSession());            
        }
        
        
        if(dataRoot.isLoaded()) 
            registerNodeEdgeHandlers(dataRoot.getApps(),false);
        
        //ErrBuffer.out("cfg menu");
        registerConfigMenu();
        //ErrBuffer.out("session listeners");
        registerSessionListeners();
    }catch(Exception ex)
            {
        ErrBuffer.err("Errors while atcivating the plugin: " + ex.getLocalizedMessage());
    }    
        //ErrBuffer.out("done start");
        //ApplicationSetup.test();
        ErrBuffer.showIfErr();
    }
    
    private void registerConfigMenu() {
            CyAppAdapter adapter = getService(bc, CyAppAdapter.class);
            CySwingAppAdapter swAdapter = getService(bc, CySwingAppAdapter.class);

           //check whether we have this one already
            JMenu appsMenu = swAdapter.getCySwingApplication().getJMenu(MenuAction.preferredMenuStr);
            if(null !=  appsMenu) {
                for(int i = appsMenu.getItemCount()-1; i>=0; --i) {
                    if(appsMenu.getItem(i)!=null && appsMenu.getItem(i).getText()!=null) {
                        if( appsMenu.getItem(i).getText().equals(MenuAction.configLabelStr) || appsMenu.getItem(i).getText().equals(OMenuAction.configLabelStr)) {
                            //System.out.println("remove: " + appsMenu.getItem(i).getText());
                            appsMenu.remove(i);
                        }
                    }
                }
            } 
            MenuAction menuA = new MenuAction(adapter);
            menuA.setManager(dataRoot);
            swAdapter.getCySwingApplication().addAction(menuA);
            swAdapter.getCySwingApplication().addAction(new OMenuAction(adapter));
    }
    
    public void stopStart() {
            this.stop(bc); //we are stopping this entire bundle
            try { this.start(bc); } catch (Exception ex) {  Logger.getLogger(CyActivator.class.getName()).log(Level.SEVERE, null, ex); }
    }
    public void registerNodeEdgeHandlers(ArrayList<String> rawApps, boolean isStop) {
        Properties myNodeViewContextMenuFactoryProps = new Properties();
        myNodeViewContextMenuFactoryProps.put("preferredMenu", "CyToStruct");

        runner = new GenericRunner();
        runner.init(rawApps);

       ErrBuffer.out("registerNodeEdgeHandlers with: "); ErrBuffer.out(new Boolean(isStop).toString());
        if(isStop) {
            stopStart();
            return;
        }
        
        //ErrBuffer.out("reg nodes");
        for (Object appCaption : runner.getMapNodes().keySet()) {
            ApplicationSetup app = (ApplicationSetup) runner.getMapNodes().get(appCaption);
            Object handler = null;

            if(app!=null && dataRoot != null)
                app.setDir(dataRoot.getDir());

            if(app!=null && app.isForNode()) 
                 handler = new NodeViewContextMenuBase(dataRoot,runner,appCaption.toString());
            else if(app!=null && app.isForEdge()) 
                handler = new EdgeViewContextMenuBase(dataRoot,runner,appCaption.toString());
            

            registerAllServices(bc, handler, myNodeViewContextMenuFactoryProps);
        }  
        ErrBuffer.out("reg edges");
        for (Object appCaption : runner.getMapEdges().keySet()) {
            ApplicationSetup app = (ApplicationSetup) runner.getMapEdges().get(appCaption);
            Object handler = null;
           if(app!=null) {
                ErrBuffer.out("dataRoot.getDir() -> " + dataRoot.getDir());
                app.setDir(dataRoot.getDir());            
           }
            if(app!=null && app.isForNode())
                handler = new NodeViewContextMenuBase(dataRoot,runner,appCaption.toString());
            else if(app!=null && app.isForEdge()) {
                handler = new EdgeViewContextMenuBase(dataRoot,runner,appCaption.toString());
            }
            registerAllServices(bc, handler, myNodeViewContextMenuFactoryProps);
        }         
        
        //edge =====================================
        Properties myEdgeViewContextMenuFactoryProps = new Properties();
        myEdgeViewContextMenuFactoryProps.put("preferredMenu", "Run");

        dataRoot.setApps(rawApps, false);
      //  CyEdgeViewContextMenuFactory myEdgeViewContextMenuFactory = new MyEdgeViewContextMenuFactory(dataRoot);
        //registerAllServices(bc, myEdgeViewContextMenuFactory, myEdgeViewContextMenuFactoryProps);
    }

    private void registerSessionListeners() {
        registerService(bc, dataRoot, SessionAboutToBeSavedListener.class, new Properties());
        registerService(bc, dataRoot, SessionLoadedListener.class, new Properties());
    }
        
    /*
    @Override
    public void bundleChanged(BundleEvent be) {
        String type = be.toString();
    }
    */
/*
    //register properties
    public void service(String path) {
        Properties molPathProps = new Properties();
        molPathProps.setProperty(ConfigManager.DATA_ROOT_PROP_NAME, path);
        SimpleCyProperty molPathProperty = new SimpleCyProperty(ConfigManager.DATA_ROOT_PROP_NAME, molPathProps, String.class, CyProperty.SavePolicy.SESSION_FILE_AND_CONFIG_DIR);
        registerService(bc, molPathProperty, CyProperty.class, new Properties());
    }
*/
    
        public class OMenuAction extends AbstractCyAction {

    final public static String preferredMenuStr = "Apps";
    final public static String configLabelStr = "Show debug log for CyToStruct";

    public OMenuAction(CyAppAdapter adapter) {
        super(configLabelStr,
                adapter.getCyApplicationManager(),
                "network",
                adapter.getCyNetworkViewManager());
        setPreferredMenu(preferredMenuStr);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFrame f = new ErrBuffer.OutDialog(ErrBuffer.getDebug());
        f.setAlwaysOnTop(true);
    }
}

    
}
