//for a more robust solution check
//from http://www.java2s.com/Tutorial/Java/0120__Development/Helpermethodtoexecuteshellcommand.htm
package org.cytoscape.app.internal;

import org.cytoscape.app.internal.core.ErrBuffer;
import org.cytoscape.app.internal.core.Utils;

import java.io.File;
import java.io.IOException;


/**
 * Utility methods for executing commands and scripts on unix and windows
 */
class TryRun {

    static Process exec(String[] cmdarray) throws IOException {
        if (isWindows()) {
            return execWindows(cmdarray);
        }
        return execUnix(cmdarray);

    }

    private static Process execUnix(String[] cmdarray) throws IOException {
        // instead of calling command directly, we'll call the shell
        Runtime rt = Runtime.getRuntime();
        File rDir = new File(System.getProperty("user.dir")).getAbsoluteFile();
        String cmd = cmdarray[0] + " " + cmdarray[1]; //should be exactly 2 elements
        String path = Utils.writeToTempAndGetPath("cd " + rDir.getAbsolutePath() + " \n " + cmd, "run_", "sh");
        if (path.isEmpty()) {
            ErrBuffer.err("Could not produce script");
            return null;
        }
        String[] cmdA = {"sh", path};
        ErrBuffer.out("executing: '" + cmdA[0] + "' '" + cmdA[1] + "' @" + rDir.getAbsolutePath());
        return rt.exec(cmdA);
    }

    private static boolean isWindows() {
        String os = System.getProperty("os.name");
        return (os != null && os.startsWith("Windows"));
    }

    private static Process execWindows(String[] cmdarray) throws IOException {
        String cmd = cmdarray[0] + " " + cmdarray[1]; //should be exactly 2 elements
        File rDir = new File(System.getProperty("user.dir")).getAbsoluteFile();
        ProcessBuilder pb = new ProcessBuilder("cmd", "/C", cmdarray[0], cmdarray[1]);
        pb.directory(rDir);

        ErrBuffer.out("executing: '" + cmd + "' @ " + rDir.getAbsolutePath());
        return pb.start();
    }

    //static boolean linked = false; // true after System.loadLibrary() is called
}
