package org.cytoscape.app.internal;

import java.awt.event.ActionEvent;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.application.swing.AbstractCyAction;

public class MenuAction extends AbstractCyAction {

    final public static String preferredMenuStr = "Apps";
    final public static String configLabelStr = "Configure CyToStruct (external programs runner)";
    private ConfigManager dataRoot;
    private Object cfgDlg;

    public MenuAction(CyAppAdapter adapter) {
        super(configLabelStr,
                adapter.getCyApplicationManager(),
                "network",
                adapter.getCyNetworkViewManager());

        setPreferredMenu(preferredMenuStr);
    }

    public void setManager(ConfigManager dataRoot) {
        this.dataRoot = dataRoot;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        cfgDlg = new ConfigDialogDN(dataRoot);
    }
}
