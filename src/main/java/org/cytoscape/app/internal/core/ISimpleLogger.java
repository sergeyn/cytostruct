package org.cytoscape.app.internal.core;

public interface ISimpleLogger {
    public void err(String s);
    public void out(String s );
}
