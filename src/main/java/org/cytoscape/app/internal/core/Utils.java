package org.cytoscape.app.internal.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.JOptionPane;

public class Utils {

    private static void err(String s) { ErrBuffer.err(s);  }

    public static File getTmpFile(String prefix, String suffix) {
        File dr = new File(System.getProperty("java.io.tmpdir"), "cytoTmpScripts");
        if (dr.exists() || dr.mkdir()) {
            try {
                return File.createTempFile(prefix+"_scr_", "." + suffix, dr);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Could not work with tmp dir: " + dr.getAbsolutePath());
            }
        }
        return null;
    }

    public static String writeToTempAndGetPath(String text, String prefix, String suffix) {
        File out = getTmpFile(prefix, suffix);
        if (out == null) {
            err("writeToTempAndGetPath failed for " + prefix + " and " + suffix);
            return "";
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(out));
            writer.write(text);
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            err("writeToTempAndGetPath failed");
        }
        return out.getAbsolutePath();
    }

    static public String safeGetString(Map m, String key) {
        return safeGetString(m, key, "");
    }

    static public String safeGetString(Map m, String key, String defaultV) {
        return (m.containsKey(key)) ? m.get(key).toString() : defaultV;
    }

    static public String joinArrayString(List<String> arr) {
        final String sep = System.getProperty("line.separator");
        return String.join(sep, arr);
    }

    static public ArrayList<String> auxReadAllLines(InputStreamReader streamR, String fname) {
        String line;
        ArrayList<String> lines = new ArrayList<String>();
        BufferedReader in;

        try {
            in = new BufferedReader(streamR);
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException ex) {
            err("Could not read from file " + fname);
        }
        return lines;

    }

    static public ArrayList<String> readAllLinesGz(File f) {
        try {
            InputStreamReader in = new InputStreamReader(new GZIPInputStream(new FileInputStream(f)));
            return auxReadAllLines(in, f.getAbsolutePath());
        } catch (IOException ex) {
            err("Could not read from gzipped file " + f.getAbsolutePath());
        }

        return new ArrayList<String>();
    }

    static public ArrayList<String> readAllLinesZip(File f, String innerFile) {
        try {
            // open the zip file stream
            InputStream theFile = new FileInputStream(f.getAbsoluteFile());
            ZipInputStream stream = new ZipInputStream(theFile);
            ZipEntry entry = null;
            while (null != (entry = stream.getNextEntry())) {
                if (entry.getName().equals(innerFile)) //a match!
                {
                    return auxReadAllLines(new InputStreamReader(stream), f.getAbsolutePath() + " / " + innerFile);
                }
            }
        } catch (IOException ex) {
            err("Could not read from file " + f.getAbsolutePath() + " / " + innerFile);
        }

        return new ArrayList<String>();
    }

    static public ArrayList<String> readAllLinesZip(File f, int entryNum) {
        try {
            // open the zip file stream
            InputStream theFile = new FileInputStream(f.getAbsoluteFile());
            ZipInputStream stream = new ZipInputStream(theFile);
            ZipEntry entry;
            for(int i=0; i<entryNum; ++i) {
                entry = stream.getNextEntry();
                        if(null == entry)
                            return new ArrayList<String>();
            }
            return auxReadAllLines(new InputStreamReader(stream), f.getAbsolutePath() + " @ " + entryNum);
        } catch (IOException ex) {
            err("Could not read from file " + f.getAbsolutePath() + " @ " + entryNum);
        }
        return new ArrayList<String>();
    }

    static public ArrayList<String> readAllLines(File f) {
        InputStreamReader in;
        try {
            in = new InputStreamReader(new FileInputStream(f));
            return auxReadAllLines(in, f.getAbsolutePath());
        } catch (FileNotFoundException ex) {
            err("Could not read from " + f.getAbsolutePath());
        }

        return new ArrayList<String>();
    }

    public static ArrayList<String> StringToLines(String str) {
        ArrayList<String> lines = new ArrayList<String>();
        try {
            BufferedReader rdr = new BufferedReader(new StringReader(str));
            for (String line = rdr.readLine(); line != null; line = rdr.readLine()) {
                lines.add(line);
            }
            return lines;
        } catch (IOException ex) {
        }
        return lines;
    }
}
