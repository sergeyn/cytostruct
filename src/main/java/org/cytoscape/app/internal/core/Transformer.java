package org.cytoscape.app.internal.core;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

class Transformer {

    final private ApplicationSetup application;
    private Map<String, ArrayList<String[]>> labelToData;
    private Map<String, Integer> headerMap;
    private TransformTree transformTree;
    private ISimpleLogger logger;

    private final static String splittingRegex
            = "(?<=%V_START%|%V_END%|%H_START%|%H_END%|%N_START%|%N_END%)|(?=%V_START%|%V_END%|%H_START%|%H_END%|%N_START%|%N_END%)";
    private final static String TAG_ST = "_START%";
    private final static String TAG_EN = "_END%";
    final static String H_START = "%H" + TAG_ST;
    final static String H_END = "%H" + TAG_EN;
    final static String V_START = "%V" + TAG_ST;
    final static String V_END = "%V" + TAG_EN;
    final static String N_START = "%N" + TAG_ST;
    final static String N_END = "%N" + TAG_EN;

    static void err(String s) {
        System.err.println(s);
        //logger.err(s);
    }

    Transformer(ApplicationSetup app, ISimpleLogger lg) {
        logger = lg;
        application = app;
        transformTree = null;
        tryAndLoadDataMatrix();
    }

    private static String getExt(String fileName) {
        int i = fileName.lastIndexOf('.');
        return (i > 0) ? fileName.substring(i + 1) : "";
    }

    private void tryAndLoadDataMatrix() {
        labelToData = new HashMap<>();
        final String fname = application.getDataMatrixFile();
        if (fname.isEmpty()) {
            return;
        }

        File dtm = new File(new File(fname).getAbsolutePath());
        if (!dtm.canRead()) {
            // try as local in dir
            dtm = new File(new File(application.getDir(), fname).getAbsolutePath());
            if (!dtm.canRead()) {
                err("couldn't read dataMatrixFile: " + dtm.getAbsolutePath());
                return;                
            }   
        }
	

        ArrayList<String> allLines;
        String ext = getExt(fname);
        if (ext.contains("zip")) {
            allLines = Utils.readAllLinesZip(dtm, application.getInnerFile());
        } else if (ext.contains("gz")) {
            allLines = Utils.readAllLinesGz(dtm);
        } else {
            allLines = Utils.readAllLines(dtm);
        }
        if (allLines.isEmpty()) {
            err("couldn't read dataMatrixFile: " + fname + " " + application.getInnerFile());
            return;
        }

        //get header
        String[] header = allLines.get(0).split(application.getColumnDelimiter());
        headerMap = new HashMap<>();
        for (int c = 0; c < header.length; ++c) {
            headerMap.put(header[c], c);
        }

        //the rest of it
        for (int i = 1; i < allLines.size(); ++i) { //each line
            String[] columns = allLines.get(i).split(application.getColumnDelimiter());
            if (columns.length != header.length) {
                err("columns.length != header.length");
                return;
            }

            if (!labelToData.containsKey(columns[0])) { //if first time
                labelToData.put(columns[0], new ArrayList<>());
            }
            labelToData.get(columns[0]).add(columns);

        }
//        String out = ("Loading data for <" + application.getAppCaption()
//                + "> DataMatrix: " + application.getDataMatrixFile()
//                + " Rows: " + labelToData.size()
//                + " Columns: " + header.length);
    }

    private static ArrayList<String[]> getMatrix(Map<String, ArrayList<String[]>> dataMatrix, String label) {
        if (!dataMatrix.containsKey(label)) {
            //ErrBuffer.out("no data in matrix for label: " + label);
            return new ArrayList<>();
        }
        return dataMatrix.get(label);
    }

    static boolean checkBlocksNestingValid(String[] blocks) {
        Stack<String> stack = new Stack<>();
        for (String s : blocks) {
            if (s.equals(H_START) || s.equals(V_START) || s.equals(N_START)) {
                stack.push(s);
            }
            if (s.equals(H_END) || s.equals(V_END) || s.equals(N_END)) {
                if (stack.empty() ||
                    (s.equals(H_END) && !stack.peek().equals(H_START)) ||
                    (s.equals(V_END) && !stack.peek().equals(V_START)) ||
                    (s.equals(N_END) && !stack.peek().equals(N_START))) {
                    return false;
                }

                stack.pop();
            }
        }
        return stack.empty();
    }

    static TransformTree.BlockType getTypeBlock(String str) {
        if (str.equals(H_START)) {
            return TransformTree.BlockType.HORIZONTAL;
        }
        if (str.equals(V_START)) {
            return TransformTree.BlockType.VERTICAL;
        }
        if (str.equals(N_START)) {
            return TransformTree.BlockType.NODES;
        }
        return TransformTree.BlockType.RAW;
    }

    // TODO: why not cached?
    static TransformTree buildBlocksTree(String[] blocks) {
        Stack<TransformTree> stack = new Stack<>();
        TransformTree root = new TransformTree("root", TransformTree.BlockType.RAW);
        stack.push(root);

        for (String s : blocks) {
            if (s.equals(H_END) || s.equals(V_END) || s.equals(N_END)) {
                stack.pop();
            } else {
                TransformTree.BlockType nextType = getTypeBlock(s);
                TransformTree lastCh = new TransformTree(s, nextType);
                stack.peek().addChild(lastCh);
                if (nextType != TransformTree.BlockType.RAW) {
                    stack.push(lastCh);
                }
            }
        }

        if (stack.size() != 1) {
            err("Something is so wrong... stack is not empty after building block tree");
        }
        return root;
    }


    private void initTransofrmTree() {
        if (!application.isBatchMode()) {
            return;
        }

        String script = application.getScriptLines();
        String[] basicBlocks = script.split(splittingRegex); //get the blocks

        if (!checkBlocksNestingValid(basicBlocks)) {
            err("**** Error: bad nesting of start/end tags");
            return;
        }

        transformTree = buildBlocksTree(basicBlocks);

    }

    private String buildScript(Map<String, String> localBinds, String label, List<String> nodeLabels) {
        if (!application.isBatchMode()) {
            return "";
        }

        TransformTree.DataPackage dp = new TransformTree.DataPackage();
        dp.arrayDelimiter = application.getArrayDelimiter();
        dp.buf = new StringBuilder();
        dp.dataMatrix = getMatrix(labelToData, label);
        dp.headerMap = headerMap;
        dp.nodeLabels = nodeLabels; // could be null

        if (null == transformTree)
            initTransofrmTree();
        if (null == transformTree) {
            err("Could not init transform tree");
            return "";
        }
        // we can have scripts even without data matrix
        //if (dp.dataMatrix.isEmpty())
        //    return "";

        transformTree.transformRoot(dp);

        //write this script to tmp file and return path
        File f = Utils.getTmpFile(label, application.getSuffix());
        if (f == null) {
            err("Could not create temp file with label: " + label);
            return "";
        }

        FileWriter bw;
        try {
            bw = new FileWriter(f);
            String finalStr = dp.buf.toString();
            //always safe to replace localBinds
            for (String key : localBinds.keySet()) {
                finalStr = finalStr.replace(key, localBinds.get(key));
            }
            bw.write(finalStr);
            bw.flush();
            bw.close();
        } catch (IOException ex) {
            err("Problems while writing script to " + f.getAbsolutePath());
        }
        return f.getAbsolutePath();
    }

    private String getCmd(Map<String, String> localBinds) {
        String cmd = application.getBinaryPathToRun() + " " + application.getCmdLine();
        for (String key : localBinds.keySet()) {
            cmd = cmd.replace(key, localBinds.get(key));
        }
        return cmd;
    }

    String[] node(List<String> nodeLabels, String dir) {
        Map<String, String> locals = new HashMap<>();
        locals.put("%dir%", dir);
        final String nodeLabel = nodeLabels.get(0);
        locals.put("%node%", nodeLabel);
        for (int i = 0; i < nodeLabels.size(); ++i) {
            locals.put("%nodes[" + i + "]%", nodeLabels.get(i));
            System.out.println("%nodes[" + i + "]% -> " + nodeLabels.get(i));
        }
        return new String[]{
                getCmd(locals), buildScript(locals, nodeLabel, nodeLabels)};
    }

    String[] edge(String edgeLabel, String node1, String node2, String dir) {
        Map<String, String> locals = new HashMap<>();
        locals.put("%dir%", dir);
        locals.put("%edge%", edgeLabel);
        locals.put("%node1%", node1);
        locals.put("%node2%", node2);
        return new String[]{
                getCmd(locals),
                buildScript(locals, edgeLabel, new ArrayList<>())};
    }
}
