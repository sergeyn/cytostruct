package org.cytoscape.app.internal.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransformTree {
    public enum BlockType {RAW, VERTICAL, HORIZONTAL, NODES}

    String text;
    BlockType blockType;
    List<TransformTree> children;

    static class DataPackage {
        StringBuilder buf;
        ArrayList<String[]> dataMatrix;
        Map<String, Integer> headerMap;
        List<String> nodeLabels;
        String arrayDelimiter;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(text);
        sb.append(" -- ");
        sb.append(blockType);
        sb.append("\n");
        for(TransformTree t : children) {
            sb.append(t.toString());
        }
        return sb.toString();
    }

    TransformTree(String t, BlockType isrvhn) {
        text = t;
        blockType = isrvhn;
        children = new ArrayList<>();
    }

    // temporary hacking to allow multi-node
    private void replaceNodeWithNodeI(int i) {
        text = text.replace("%node%", "%nodes[" + i + "]%");
    }

    private void replaceNodeIWithNode(int i) {
        text = text.replace("%nodes[" + i + "]%", "%node%");
    }

    private HashMap<String, String[]> getArrays(DataPackage dp, int row) {
        HashMap<String, String[]> arrays = new HashMap<>();
        if (dp.headerMap == null || dp.headerMap.isEmpty())
            return arrays;
        for (String column : dp.headerMap.keySet()) { //scan columns for arrays present in block
            if (text.contains(column)) {
                String rawValueForKey = dp.dataMatrix.get(row)[dp.headerMap.get(column)];
                String[] values = rawValueForKey.split(dp.arrayDelimiter);
                if (values.length > 1) { //an internal array
                    arrays.put(column, values);
                }
            }
        }
        return arrays;
    }

    private void rawTransform(DataPackage dp, int row, int col) {
        StringBuilder buf = new StringBuilder();
        HashMap<String, String[]> arrays = getArrays(dp, row);

        if (!arrays.isEmpty()) { //first handle arrays unwinding:
            String blockToReplace = text;
            if (col == -1) { //raw -- don't unwind at all
                for (String column : arrays.keySet()) {
                    blockToReplace = blockToReplace.replace("%" + column + "%", dp.dataMatrix.get(row)[dp.headerMap.get(column)]); //get the entire array
                }
                buf.append(blockToReplace);
            }
            else {
                for (String column : arrays.keySet()) {
                    blockToReplace = blockToReplace.replace("%" + column + "%", arrays.get(column)[col]);
                }
                buf.append(blockToReplace);
            }
        } //end if there are arrays at all

        String firstStage = (buf.length() > 0) ? buf.toString() : text;
        if (dp.headerMap != null && !dp.headerMap.isEmpty())
            for (String column : dp.headerMap.keySet()) {
                if (firstStage.contains(column)) {
                    String rawValueForKey = dp.dataMatrix.get(row)[dp.headerMap.get(column)];
                    String[] values = rawValueForKey.split(dp.arrayDelimiter);
                    if (values.length == 1) { //not an array
                        firstStage = firstStage.replace("%" + column + "%", values[0]);
                    }
                }
            }
        dp.buf.append(firstStage);
    }

    void transformRoot(DataPackage dp) {
        for (TransformTree t : children) {
            t.transform(dp, 0, -1);
        }
    }

    private void transform(DataPackage dp, int row, int col) {
        if (blockType == BlockType.RAW) {
            rawTransform(dp, row, col);
        } else if (blockType == BlockType.VERTICAL) {
            for (int i = 0; i < dp.dataMatrix.size(); ++i) {
                for (TransformTree t : children) {
                    t.transform(dp, i, col);
                }
            }
        } else if (blockType == BlockType.NODES) {
            for (int i = 0; i < dp.nodeLabels.size(); ++i) {
                for (TransformTree t : children) {
                    t.replaceNodeWithNodeI(i);
                    t.transform(dp, row, col);
                    t.replaceNodeIWithNode(i);
                }
            }
        } else if (blockType == BlockType.HORIZONTAL) {
            int arraySz = -1;
            for (TransformTree t : children) {
                if (!t.getArrays(dp, row).isEmpty()) {
                    arraySz = t.getArrays(dp, row).values().iterator().next().length;
                    break;
                }
            }
            if (-1 != arraySz) { //first handle arrays unwinding:
                for (int i = 0; i < arraySz; ++i) {
                    for (TransformTree t : children) {
                        t.transform(dp, row, i);
                    }
                }
            } // end if had arrays
            else {
                for (TransformTree t : children) {
                    t.transform(dp, row, col);
                }
            }
        }
    }

    void addChild(TransformTree newT) {
        children.add(newT);
    }
}
