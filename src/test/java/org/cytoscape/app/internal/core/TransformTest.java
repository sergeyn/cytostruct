package org.cytoscape.app.internal.core;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TransformTest {
    @Test
    public void getTypeBlockHandlesAllCases() {
        assertEquals(
                TransformTree.BlockType.HORIZONTAL,
                Transformer.getTypeBlock(Transformer.H_START));
        assertEquals(
                TransformTree.BlockType.VERTICAL,
                Transformer.getTypeBlock(Transformer.V_START));
        assertEquals(
                TransformTree.BlockType.NODES,
                Transformer.getTypeBlock(Transformer.N_START));
        assertEquals(
                TransformTree.BlockType.RAW,
                Transformer.getTypeBlock("anything"));

    }

    @Test
    public void blockNestingValidForEmptyAndRaw() {
        String[] empty = {};
        assertTrue(Transformer.checkBlocksNestingValid(empty));
        String[] raw = { "abcd\nxyz\n%FAKE%"};
        assertTrue(Transformer.checkBlocksNestingValid(raw));
    }

    @Test
    public void blockNestingValidForOneLevel() {
        String[] vblock = {Transformer.V_START, "raw-block\nfoo", Transformer.V_END};
        assertTrue(Transformer.checkBlocksNestingValid(vblock));

        String[] hblock = {Transformer.H_START, "raw-block\nbar", Transformer.H_END};
        assertTrue(Transformer.checkBlocksNestingValid(hblock));

        String[] nblock = {Transformer.N_START, "raw-block\nbaz", Transformer.N_END};
        assertTrue(Transformer.checkBlocksNestingValid(nblock));
    }

    @Test
    public void blockNestingInvalidForOneLevel() {
        String[] vblock = {Transformer.V_START, "raw-block\nfoo", Transformer.H_END};
        assertFalse(Transformer.checkBlocksNestingValid(vblock));

        String[] hblock = {Transformer.H_START, "raw-block\nbar", Transformer.V_END};
        assertFalse(Transformer.checkBlocksNestingValid(hblock));

        String[] nblock = {Transformer.N_START, "raw-block\nbaz", Transformer.H_END};
        assertFalse(Transformer.checkBlocksNestingValid(nblock));

        String[] vblockp = {Transformer.V_START, "raw-block\nfoo"};
        assertFalse(Transformer.checkBlocksNestingValid(vblockp));

        String[] hblockp = {Transformer.H_START, "raw-block\nbar"};
        assertFalse(Transformer.checkBlocksNestingValid(hblockp));

        String[] nblockp = {Transformer.N_START, "raw-block\nbaz"};
        assertFalse(Transformer.checkBlocksNestingValid(nblockp));
    }

    @Test
    public void blockNestingValidForNested() {
        String[] nested = {
                Transformer.V_START, "foo", Transformer.H_START, "bar", Transformer.N_START,
                "open %node%", Transformer.N_END, Transformer.H_END, Transformer.V_END};
        assertTrue(Transformer.checkBlocksNestingValid(nested));

        String[] badlyNested = {
                Transformer.V_START, "foo", Transformer.H_START, "bar", Transformer.N_START,
                "open %node%", Transformer.V_END, Transformer.H_END, Transformer.N_END};
        assertFalse(Transformer.checkBlocksNestingValid(badlyNested));
    }

    @Test
    public void correctTreeFromBlocks() {
        String[] nested = {
                Transformer.V_START, "foo\nfoo", Transformer.H_START, "bar\nbar", Transformer.N_START,
                "open %node%", Transformer.N_END, Transformer.H_END, Transformer.V_END};
        TransformTree root = Transformer.buildBlocksTree(nested);
        assertEquals(TransformTree.BlockType.RAW, root.blockType);
        assertEquals("root", root.text);

        List<TransformTree> children = root.children;
        assertEquals(1, children.size());

        TransformTree vb = children.get(0);
        assertEquals(TransformTree.BlockType.VERTICAL, vb.blockType);
        assertEquals(Transformer.V_START, vb.text);

        children = vb.children;
        assertEquals(2, children.size());

        TransformTree foo = children.get(0);
        assertEquals(TransformTree.BlockType.RAW, foo.blockType);
        assertEquals("foo\nfoo", foo.text);

        TransformTree hb = children.get(1);
        assertEquals(TransformTree.BlockType.HORIZONTAL, hb.blockType);
        assertEquals(Transformer.H_START, hb.text);

        children = hb.children;
        assertEquals(2, children.size());

        TransformTree bar = children.get(0);
        assertEquals(TransformTree.BlockType.RAW, bar.blockType);
        assertEquals("bar\nbar", bar.text);

        TransformTree nb = children.get(1);
        assertEquals(TransformTree.BlockType.NODES, nb.blockType);
        assertEquals(Transformer.N_START, nb.text);

        children = nb.children;
        assertEquals(1, children.size());

        TransformTree node = children.get(0);
        assertEquals(TransformTree.BlockType.RAW, node.blockType);
        assertEquals("open %node%", node.text);


        String s = root.toString();
        assertNotNull(s);
    }

}
