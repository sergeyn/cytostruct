# README #

CyToStruct -- [Cytoscape](http://www.cytoscape.org/) plugin for executing external applications in command or batch mode.
Mainly targets programs for visualizing molecular structures ([PyMOL](http://www.pymol.org/), [Jmol](http://jmol.sourceforge.net/), [Chimera](https://www.cgl.ucsf.edu/chimera/), etc.).

For more information consult our [Wiki](https://bitbucket.org/sergeyn/cytostruct/wiki/Home)